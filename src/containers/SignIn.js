import React from "react";
import {Button, Checkbox, Form, Icon, Input, message} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Amplify, { Auth } from 'aws-amplify';
import { Layout,Card,notification } from 'antd';
import { Row, Col } from 'antd';
import {InfoCircleOutlined} from '@ant-design/icons';
import {
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGithubSignIn,
  userGoogleSignIn,
  userSignIn,
  userTwitterSignIn
} from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignIn extends React.Component {
  constructor(props) {
		super(props);
    this.state = {
        collapsed:false,
        toDashboard:false,
        username:'',
        Group:'',
        Userinfo:'',
        email: '',
        password: '',
        passwordChallenge:false,
        mfachallenge:false,
        newPassword:'',
        coguser:{},
        verify:'',
        opassword:'',
        uemail:'',
        nnpassword:'',
        load:false,
        gload:false,
        vreset:false
    };
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.showAuthLoader();
        this.props.userSignIn(values);
      }
    });
  };
  formhandleChange=event=>{
    
    this.setState({
			[event.target.id]: event.target.value.trim()
    });
  
  
 
  }
  formsubmit=async event => {
    //event.preventDefault();
  if(this.state.email&&this.state.password)
  {
console.log("state",this.state);
    this.setState({ isLoading: true });
    this.setState({load:true});
    this.setState({
      gload: true
      // confirmLoading: false,
     });
//if(this.state.passwordChallenge==false)
		try {
    var x=	await Auth.signIn(this.state.email, this.state.password);
    console.log("res",x)
  this.setState({coguser:x});
  //alert(x.challengeName)
//localStorage.setItem("usession",JSON.stringify(x));
    if (x.challengeName === 'NEW_PASSWORD_REQUIRED'){
      this.setState({passwordChallenge: true,isLoading: false});
      this.setState({
        gload: false
        // confirmLoading: false,
       });
console.log("new password required"+JSON.stringify(x));


    }

    else if(x.challengeName==='SMS_MFA')
    { setTimeout(() => {
      this.setState({
        load: false
       // confirmLoading: false,
      });
      this.setState({
       gload: false
       // confirmLoading: false,
      });
      this.setState({mfachallenge:true})
    }, 4000);

      //var data=await Auth.currentUserInfo()
      console.log("this is a test",this.state.coguser);
     
    }
   else{
      try{
        var data=await Auth.currentUserInfo()
          console.log("this is a test",data);
          console.log("ID-token",Auth.get)
          var x=await Auth.currentSession();
          console.log("session",x)
          this.setState({Userinfo:data.attributes.email})
          const user =  await Auth.currentAuthenticatedUser();
          console.log("user",user)
          this.setState({Group:user.signInUserSession.accessToken.payload["cognito:groups"][0]})
          localStorage.setItem("userEmail",data.attributes.email)
   var x=localStorage.getItem("userEmail")
   localStorage.setItem("user",user.signInUserSession.accessToken.payload["cognito:groups"][0])
   try {
    Auth.currentAuthenticatedUser({bypassCache: true})
   const currentUserInfo = await Auth.currentUserInfo()
   const UserIdinfo = currentUserInfo.attributes['custom:UserId']
   const UserInitialsinfo = currentUserInfo.attributes['custom:UserInitials']
   const CompanyIdinfo = currentUserInfo.attributes['custom:CompanyId']
localStorage.setItem('UserIdinfo',UserIdinfo)
localStorage.setItem('UserInitialsinfo',UserInitialsinfo)
localStorage.setItem('CompanyIdinfo',CompanyIdinfo)

console.log('currentUserInfo '+UserIdinfo+'  '+UserInitialsinfo+'  '+CompanyIdinfo);




   }
   catch (err) {
    console.log('error fetching user info: ', err);
  }
  this.props.history.push({
     pathname: '/dashboard',
     userData: this.state.Userinfo,
     groupData: this.state.Group
   });
          }
          catch(e) {
            alert(e);
            this.setState({ isLoading: false });
            this.setState({gload:false})
          }
    }
		
			
			
			
		} catch (e) {
      //alert(e.message);
      notification.open({
        message: 'Alert',
        description:
        e.message
      });
      if(e.message=='Password reset required for the user')
      {
       this.setState({vreset:true})
       this.setState({gload:false})
      }
      this.setState({ isLoading: false });
      this.setState({gload:false})
    }
    setTimeout(() => {
      this.setState({
        load: false
       // confirmLoading: false,
      });
     
    }, 2000);
  }
  else{
    if(!this.state.email)
    {
    message.error("Email fields are missing");
    }
    else if(!this.state.password)
{
  message.error("Password fields are missing");
    }

  }


	};


  componentDidUpdate() {
  // console.log("email",this.state.email)
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }
  verifymfa=async ()=>
  {
    this.setState({
      gload: true
      // confirmLoading: false,
     });
   let user=this.state.coguser;
    console.log("s",this.state);
    if (user.challengeName === 'SMS_MFA'){
      try{
      var res=await Auth.confirmSignIn(this.state.coguser,this.state.verify);
      console.log("resauth",res);
      
        notification.open({
          message: 'Alert',
          description:
            'MFA Verified successfully',
        
        });


       
          var data=await Auth.currentUserInfo()
          console.log("this is a test",data);
          var x=await Auth.currentSession();
          console.log("session",x)
          this.setState({Userinfo:data.attributes.email})
          const user =  await Auth.currentAuthenticatedUser();
          console.log("user",user)
          this.setState({Group:user.signInUserSession.accessToken.payload["cognito:groups"][0]})
          localStorage.setItem("userEmail",data.attributes.email)
   var x=localStorage.getItem("userEmail")
    console.log("userEmail"+x)
    const currentUserInfo = await Auth.currentUserInfo()
    const UserIdinfo = currentUserInfo.attributes['custom:UserId']
    const UserInitialsinfo = currentUserInfo.attributes['custom:UserInitials']
    const CompanyIdinfo = currentUserInfo.attributes['custom:CompanyId']
 localStorage.setItem('UserIdinfo',UserIdinfo)
 localStorage.setItem('UserInitialsinfo',UserInitialsinfo)
 localStorage.setItem('CompanyIdinfo',CompanyIdinfo)
   setTimeout(() => {
    
    this.setState({
     gload: false
     // confirmLoading: false,
    });
   
  }, 2000);


        localStorage.setItem("user",user.signInUserSession.accessToken.payload["cognito:groups"][0])


         this.props.history.push({
           pathname: '/dashboard',
           userData: this.state.Userinfo,
           groupData: this.state.Group
         });
            }
            catch(e) {
              console.log("error",e);
              notification.open({
                message: 'Alert',
                description:
                  'MFA Verification failed',
               
              });
              this.setState({gload:false})
              this.setState({ isLoading: false });
            }
      
      
    }

  }

  changepassword=async event=>
  {
    this.setState({
      gload: true
      // confirmLoading: false,
     });
   if (this.state.coguser.challengeName === 'NEW_PASSWORD_REQUIRED'){
   // console.log("USER EMAIL"+this.props.location.state.detail.basic_Email+"old password"+this.props.location.state.detail.basic_password+"new password: "+this.state.basic_npassword);
    var res=await Auth.completeNewPassword(this.state.coguser,this.state.npassword);
    console.log("here",res)
    if(res.username)
    {
      notification.open({
        message: 'Alert',
        description:
          'Password changed successfully'
      });
     
    this.setState({passwordChallenge: false});
    //this.props.history.push("/signin");
    this.setState({
      gload: false
      // confirmLoading: false,
     });
    }
   }
   console.log("changed password"+JSON.stringify(res));
  
  }
  resetpassword=async event=>{
    this.setState({
      gload: true
      // confirmLoading: false,
     });
     if(this.state.npassword===this.state.nnpassword)
     {
       try{
       var res=await Auth.forgotPasswordSubmit(this.state.email,this.state.verify,this.state.npassword);
       console.log("err",res)
       notification.open({
        message: 'Alert',
        description:
          'Password changed successfully'
      });
      this.setState({vreset: false});
   this.setState({gload:false})
       }
       catch(err)
       {
        notification.open({
          message: 'Alert',
          description:
            err.message
        });
         
       }

     }

  }
  render() {
    const {getFieldDecorator} = this.props.form;
    const {showMessage, loader, alertMessage} = this.props;

    if(this.state.vreset)
    {
      return (
        <div className="gx-app-login-wrap">
          <div className="gx-app-login-container">
            <div className="gx-app-login-main-content">
              <div className="gx-app-logo-content">
                <div className="gx-app-logo-content-bg">
                  
                </div>
                <div className="gx-app-logo-wid">
                  <h1><IntlMessages id="app.userAuth.signIn"/></h1>
                 <p> Password Reset Form</p>
                </div>
                <div className="gx-app-logo">
                  <img alt="example" src={require("assets/images/WARD_Digital_Logo_Lockup_White.png")}/>
                </div>
              </div>
              <div className="gx-app-login-content">
                <Form autocomplete="off" onSubmit={()=>this.verifymfa(this)} className="gx-signin-form gx-form-row0">
  
                  <FormItem>
                   
                      <Input id="verify" placeholder="Enter Verification Code sent to Email" value={this.state.verify} onChange={this.formhandleChange} required/>
                    
                  </FormItem>
                  <Form.Item>
                  <Input.Password size="large"  id="npassword" placeholder="Enter new password" value={this.state.npassword} onChange={this.formhandleChange} required/>
  </Form.Item>
  <Form.Item>
  <Input.Password size="large"  id="nnpassword" placeholder="Re-Enter new password" value={this.state.nnpassword} onChange={this.formhandleChange}  required/>
  </Form.Item>
                  
                  <FormItem>
                    <Button type="primary" className="gx-mb-0"  onClick={()=>this.resetpassword(this)} onEnter={()=>this.resetpassword(this)}>
                      Reset Password
                    </Button>{this.state.gload?(<img src={require("assets/images/294.gif")} width="67"/>):<p></p>}
                
                  </FormItem>
                
                  <span
                    className="gx-text-light gx-fs-sm"> </span>
                </Form>
              </div>
  
              {loader ?
                <div className="gx-loader-view">
                  <CircularProgress/>
                </div> : null}
              {showMessage ?
                message.error(alertMessage.toString()) : null}
            </div>
          </div>
        </div>
      );
  }



    if(this.state.mfachallenge)
    {
      return (
        <div className="gx-app-login-wrap">
          <div className="gx-app-login-container">
            <div className="gx-app-login-main-content">
              <div className="gx-app-logo-content">
                <div className="gx-app-logo-content-bg">
                  
                </div>
                <div className="gx-app-logo-wid">
                  <h1><IntlMessages id="app.userAuth.signIn"/></h1>
                 <p> Please enter the multi-factor authentication (MFA) code that was sent to your registered mobile number.</p>
                </div>
                <div className="gx-app-logo">
                  <img alt="example" src={require("assets/images/WARD_Digital_Logo_Lockup_White.png")}/>
                </div>
              </div>
              <div className="gx-app-login-content">
                <Form autocomplete="off" onSubmit={()=>this.verifymfa(this)} className="gx-signin-form gx-form-row0">
  
                  <FormItem>
                   
                      <Input id="verify" placeholder="Enter Verification Code" value={this.state.verify} onChange={this.formhandleChange} required/>
                    
                  </FormItem>
                  
                  
                  <FormItem>
                    <Button type="primary" className="gx-mb-0"  onClick={()=>this.verifymfa(this)} onEnter={()=>this.verifymfa(this)}>
                      Verify
                    </Button>{this.state.gload?(<img src={require("assets/images/294.gif")} width="67"/>):<p></p>}
                
                  </FormItem>
                
                  <span
                    className="gx-text-light gx-fs-sm"> </span>
                </Form>
              </div>
  
              {loader ?
                <div className="gx-loader-view">
                  <CircularProgress/>
                </div> : null}
              {showMessage ?
                message.error(alertMessage.toString()) : null}
            </div>
          </div>
        </div>
      );
  }

  else if (this.state.passwordChallenge)
  {
    return (
      <div className="gx-app-login-wrap">
      <div className="gx-app-login-container">
        <div className="gx-app-login-main-content">
          <div className="gx-app-logo-content">
            <div className="gx-app-logo-content-bg">
              
            </div>
            <div className="gx-app-logo-wid">
              <h1><IntlMessages id="app.userAuth.signIn"/></h1>
             <p> Welcome to Wardlaw Digital Portal.</p>
            </div>
            <div className="gx-app-logo">
              <img alt="example" src={require("assets/images/WARD_Digital_Logo_Lockup_White.png")}/>
            </div>
          </div>
          <div className="gx-app-login-content">
            <Form autocomplete="off"  className="gx-signin-form gx-form-row0">
  
    <Form.Item
  
  
  >
  <Input type="email" size="large"  id="uemail" placeholder="Enter Username" value={this.state.email} onChange={this.formhandleChange}  required/>
  </Form.Item>
  
    <Form.Item
  
  
  >
  <Input.Password size="large"  id="opassword" placeholder="Enter Old Password" value={this.state.opassword} onChange={this.formhandleChange}  required/>
  </Form.Item>
  
  
    <Form.Item
  
  
  >
  <Input.Password size="large"  id="npassword" placeholder="Enter new password" value={this.state.npassword} onChange={this.formhandleChange} required/>
  </Form.Item>
  <Form.Item>
  <Input.Password size="large"  id="nnpassword" placeholder="Re-Enter new password" value={this.state.nnpassword} onChange={this.formhandleChange}  required/>
  </Form.Item>
  
  
  <Form.Item>
  <Button type="primary" className="gx-mb-0"  onClick={()=>this.changepassword(this)} onEnter={()=>this.changepassword(this)}>
                    <IntlMessages id="Change Password"/>
                  </Button>{this.state.gload?(<img src={require("assets/images/294.gif") } width="67"/>):<p></p>}
  </Form.Item>
    
  
            </Form>
          </div>
  
          {loader ?
            <div className="gx-loader-view">
              <CircularProgress/>
            </div> : null}
          {showMessage ?
            message.error(alertMessage.toString()) : null}
        </div>
      </div>
    </div>
      )

  }
else{
  return (
    <div className="gx-app-login-wrap">
      <div className="gx-app-login-container">
        <div className="gx-app-login-main-content">
          <div className="gx-app-logo-content">
            <div className="gx-app-logo-content-bg">
              
            </div>
            <div className="gx-app-logo-wid">
              <h1><IntlMessages id="app.userAuth.signIn"/></h1>
             <p> Welcome to Wardlaw Digital Portal.</p>
            </div>
            <div className="gx-app-logo">
              <img alt="example" src={require("assets/images/WARD_Digital_Logo_Lockup_White.png")}/>
            </div>
          </div>
          <div className="gx-app-login-content">
            <Form autocomplete="off"  className="gx-signin-form gx-form-row0">

              <FormItem>
               
                  <Input placeholder="Email" id="email" placeholder="Email" value={this.state.email} onChange={this.formhandleChange} required/>
                
              </FormItem>
              <FormItem>
               
                  <Input type="password" placeholder="Password" id="password" value={this.state.password} onChange={this.formhandleChange}  required/>
               
              </FormItem>
              
              <FormItem>
                <Button type="primary" className="gx-mb-0"  onClick={()=>this.formsubmit(this)} onEnter={()=>this.formsubmit(this)}>
                  <IntlMessages id="app.userAuth.signIn"/>
                </Button>{this.state.gload?(<img src={require("assets/images/294.gif")} width="67"/>):<p></p>}
            
              </FormItem>
              <div className="gx-flex-row gx-justify-content-between">
                <span>or connect with</span>
                <ul className="gx-social-link" style={{marginRight: "193px",marginTop: "-9px"}}>
                  <li>
                    <Icon type="google" onClick={() => {
                      this.props.showAuthLoader();
                      this.props.userGoogleSignIn();
                    }}/>
                  </li>
                 
                </ul>
              </div>
              <span
                className="gx-text-light gx-fs-sm"> </span>
            </Form>
          </div>

          {loader ?
            <div className="gx-loader-view">
              <CircularProgress/>
            </div> : null}
          {showMessage ?
            message.error(alertMessage.toString()) : null}
        </div>
      </div>
    </div>
  );

}

}

}

const WrappedNormalLoginForm = Form.create()(SignIn);

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser} = auth;
  return {loader, alertMessage, showMessage, authUser}
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGoogleSignIn,
  userGithubSignIn,
  userTwitterSignIn
})(WrappedNormalLoginForm);
