import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import {Layout,Select, Popover,Input,Avatar,message,Modal} from "antd";
import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import axios from 'axios';
import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { YahooFilled,BankOutlined,CloudSyncOutlined,AuditOutlined,FunnelPlotOutlined,SolutionOutlined,ToolOutlined,SettingOutlined} from "@ant-design/icons";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
class SidebarContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
   
  
      companyId:'',
      func:this.customfunction
     
    }
  }
  async componentDidMount() {
    
    var user=localStorage.getItem('user');
    localStorage.setItem('com',this.props.sprops.cstate.company);
    var gdisplay='';
    console.log("group",user);
    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/role`)
        //console.log("datafrom Api",res);
        res.data.Items.forEach(function(obj) {
          
          console.log("datafrom2 Api",obj);
       if(user==obj.Name)
       {
gdisplay=obj.DisplayName;
       }
    
       
          
        
        });
     console.log("dname"+gdisplay);
    
       this.setState({Group:gdisplay})
       
       var x=gdisplay.split(' ');
       console.log("length",x);
       if(x.length>1)
       {
       this.setState({fn:x[0].charAt(0)});
       this.setState({ln:x[1].charAt(0)});
       }
       else{
        this.setState({fn:gdisplay.charAt(0)});

       }
     
 
  


 }
  customfunction()
  {
    alert("this is a test")
  }
  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
access=event=>{
  event.preventDefault()
  let secondsToGo = 5;
  const modal = Modal.warning({
    title: 'Access Denied',
    content: `You are not authorized to access this page. Please contact your company admin for assistance.`,
  });
  const timer = setInterval(() => {
    secondsToGo -= 1;
    modal.update({
      content: `You are not authorized to access this page. Please contact your company admin for assistance.`,
    });
  }, 1000);
  setTimeout(() => {
    clearInterval(timer);
    modal.destroy();
  }, secondsToGo * 1000);
}
  render() {
    const {themeType, navStyle, pathname,sprops} = this.props;
    localStorage.setItem('com',this.props.sprops.cstate.company);
    console.log("phere",sprops.cstate);
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>
      <SidebarLogo/>
      <div className="gx-sidebar-content">
        
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">

            
              <Menu.Item key="main/widgets">
                <Link to={{
  pathname: '/dashboard',
  state: {
    ssprops: sprops.cstate.company
  }
}}><i className="icon icon-dasbhoard"/>
                       <IntlMessages id="sidebar.dashboard"/></Link>
              </Menu.Item>
              <SubMenu key="dashboard12" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span>   <i className="icon icon-widgets"/>
                         <IntlMessages id="Onboarding"/></span>}>
                         <Menu.Item key="main/layouts12">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    nav:'Single Send',
    form: true
  }
}}><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
              <Menu.Item key="main/layouts11">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    nav:'Bulk Send',
    form: false
  }
}}><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>
         
              <Menu.Item key="main/layouts4">
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>
              </SubMenu>
             
              <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">

            
              <Menu.Item key="main/widgets">
                <Link to={{
  pathname: '/claims',
  state: {
    action:"Smart Claims Program"
  }
}}><i className="icon icon-wysiwyg"/>
                       <IntlMessages id="Claims"/></Link>
              </Menu.Item>
              </Menu>
              <SubMenu key="dashboard" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <CloudSyncOutlined style={{fontSize:"20px",    paddingTop: "10px"}}/>
                         <IntlMessages id="Renewal"/></span>}>
                         <Menu.Item key="main/layouts2">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    nav:'Single Send',
    form: true
  }
}}><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
              <Menu.Item key="main/layouts1">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    nav:'Bulk Send',
    form: false
  }
}}><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>
          
              <Menu.Item key="main/layouts4">
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>
              </SubMenu>
           
            
            
            
                           <SubMenu key="dashboard21" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <CloudSyncOutlined style={{fontSize:"20px",    paddingTop: "10px"}}/>
                         <IntlMessages id="CAT Notifications"/></span>}>
                         <Menu.Item key="main/layouts22">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    nav:'Single Send',
    form: true
  }
}}><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
              <Menu.Item key="main/layouts23">
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    nav:'Bulk Send',
    form: false
  }
}}><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>
          
                <Menu.Item key="main/layouts4">
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>
              </SubMenu>
           
            
              <SubMenu key="dashboard4" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <FunnelPlotOutlined  style={{fontSize:"20px",    paddingTop: "10px"}}/>
                         <IntlMessages id="Reports"/></span>}>
                         <Menu.Item key="components/navigation/affix">
                         <Link to={{
  pathname: '/report',
  state: {
   rtype:'Unsubscribed'
  }
}}><SolutionOutlined style={{fontSize:"16px",    paddingTop: "10px"}}/>
                                                          <IntlMessages
                                                            id="Unsubscribed"/></Link>
                              </Menu.Item>
                              
                           </SubMenu>
                           <Menu.Item key="components/navigation/affix2">
                <Link to="/main/metrics"><SettingOutlined  style={{fontSize:"20px",    paddingTop: "10px"}} />
                    <IntlMessages
                      id="Settings"/></Link>
                </Menu.Item>
                <SubMenu
                key="company"
                className={this.getNavStyleSubMenuClass(navStyle)}
                title={
                  <span>
                    {" "}
                    <BankOutlined
                      style={{ fontSize: "20px", paddingTop: "10px" }}
                    />
                    <IntlMessages id="Company" />
                  </span>
                }
              >
                <Menu.Item key="main/layouts1">
                {localStorage.getItem('user')=='WARD-Super-Admin' ? <Link
                    to={{
                      pathname: "/create"
                    }}
                  >
                    <i className="icon icon-add-circle" />
                    <IntlMessages id="Create" />
                  </Link>:
                  <Link to="/"
                  onClick={ this.access } >
                  <i className="icon icon-add-circle" />
                  <IntlMessages id="Create" />
                </Link>}
                </Menu.Item>
                <Menu.Item key="main/layouts2">
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin' ?   <Link
                    to={{
                      pathname: "/manage"
                    }}
                  >
                    <i className="icon icon-ckeditor" />
                    <IntlMessages id="Manage" />
                  </Link>:
                   <Link
                   to="/"
                   onClick={ this.access }
                 >
                   <i className="icon icon-ckeditor" />
                   <IntlMessages id="Manage" />
                 </Link>}
                </Menu.Item>
              </SubMenu>
                
              <SubMenu
                key="User"
                className={this.getNavStyleSubMenuClass(navStyle)}
                title={
                  <span>
                    {" "}
                    <i className="icon icon-user"/>
                    <IntlMessages id="User" />
                  </span>
                }
              >
                <Menu.Item key="main/layouts31">
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin'  ? <Link
                    to={{
                      pathname: "/User",
                      state: {
                        action:"Create"
                      }
                    }}
                  >
                    <i className="icon icon-add-circle" />
                    <IntlMessages id="Create" />
                  </Link>:
                  <Link to="/"
                  onClick={ this.access }
                   >
                  <i className="icon icon-add-circle" />
                  <IntlMessages id="Create" />
                </Link>}
                </Menu.Item>
                <Menu.Item key="main/layouts2">
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin' ?  <Link
                    to={{
                      pathname: "/User",
                      state: {
                        action:"Manage"
                      }
                    }}
                  >
                    <i className="icon icon-ckeditor" />
                    <IntlMessages id="Manage" />
                  </Link>:
                   <Link
                   to="/"
                   onClick={ this.access }
                 >
                   <i className="icon icon-ckeditor" />
                   <IntlMessages id="Manage" />
                 </Link>
                   }
                </Menu.Item>
              </SubMenu>
                

              
            
                
              
             

            

           

          </Menu>
        </CustomScrollbars>
      </div>
    </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({settings}) => {
  const {navStyle, themeType, locale, pathname} = settings;
  return {navStyle, themeType, locale, pathname}
};
export default connect(mapStateToProps)(SidebarContent);

