import React, {Component} from "react";

import {Layout,Select, Popover,Input,Avatar,Menu,Badge,Button} from "antd";
import {Link,Redirect} from "react-router-dom";
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import CustomScrollbars from "util/CustomScrollbars";
import languageData from "./languageData";
import apiCall from '../../apiutil/apicall';
import {switchLanguage, toggleCollapsedSideNav} from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import AppNotification from "components/AppNotification";
import MailNotification from "components/MailNotification";
import Auxiliary from "util/Auxiliary";
import axios from 'axios';
import './index.css';
import {NAV_STYLE_DRAWER, NAV_STYLE_FIXED, NAV_STYLE_MINI_SIDEBAR, TAB_SIZE} from "../../constants/ThemeSetting";
import {connect} from "react-redux";
import { userFacebookSignIn } from "../../appRedux/actions/Auth";
import {
  MenuUnfoldOutlined,UserAddOutlined,
  MenuFoldOutlined,user,PlusCircleFilled,FileSearchOutlined,
  UserOutlined,LogoutOutlined,PlusCircleOutlined,
  VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,
  UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
} from '@ant-design/icons';
const {Header} = Layout;
const { SubMenu } = Menu;

class Topbar extends Component {
  constructor(props) {
		super(props);
  this.state = {
    searchText: '',
    company:[],
    gcheck:true,
    fname:'',
    lname:'',
    users:[],
    susers:[],
    logoimg:''
    
  };
}

  async componentDidMount() {

    /*setTimeout(function() {
      console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
      localStorage.clear();
    
      try{
        window.location.href = '/signin'
      }
      catch(es)
      {
        console.log("es",es)
      }
    
     
  }, 20 * 60 * 1000);
*/
try{
    var userid=localStorage.getItem('UserIdinfo')
    var fname=localStorage.getItem('UserInitialsinfo').split(' ')
    //console.log("qqq",localStorage.getItem('CompanyIdinfo'));
    //localStorage.setItem('CompanyIdinfo'

    if(localStorage.getItem('user')== 'WARD-Company-Admin')
    {
      var res1= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/company/`+localStorage.getItem('CompanyIdinfo'))
      console.log('res1',res1);
          var x=[];
          res1.data.Items.forEach(function(obj) {
    
    x.push(obj);
          })
          this.setState({susers:x}); 
          
    }
   if(localStorage.getItem('user')!= 'WARD-Super-Admin')
   {
    this.setState({gcheck:true})
    console.log("here"+this.state.gcheck);
    
    var x='';
    var fname;
    var lname;
    var uid='';

    var comid=localStorage.getItem('CompanyIdinfo');
     var res=await apiCall.getApi(`/company/getlogo/${comid}`);
    this.setState({logoimg:"data:image/*;base64," + res.data.base64 })
    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/${userid}`)
       
       /* res.data.Items.forEach(function(obj) {
          if(localStorage.getItem('userEmail')===obj.EmailAddress)
          {
          console.log("datafrom3 Api",obj);
          localStorage.setItem("userId",obj.UserId);
        x=obj.CompanyId
        fname=obj.FirstName;
        lname=obj.LastName;
        //localStorage.setItem("firstname",obj.FirstName);
        //localStorage.setItem("lastname",obj.LastName);
        //localStorage.setItem("uuid",obj.UserId);
          } 
        
        });*/
        

 x=res.data.Item.CompanyId

        var com='';
   //https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/9a87dc93-e965-4878-94f0-8d966e1beea9
   console.log(x);
   var res2= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/${x}`)
   console.log("company Api",res2.data.Item);
   
   com=res2.data.Item.Name;
    
  
   var acom=[]
   acom.push(com);
   this.setState({company:acom})
   
   this.setState({gcheck:true})
   this.props.func(x)
   this.setState({fname:fname[0]});
   this.setState({lname:fname[1]});
   localStorage.setItem('fname',fname[0])
   localStorage.setItem('lname',fname[1])

      }
      else{
        var company=this.state.company;
        var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company`)
        console.log("company datafrom Api",res.data);
        res.data.Items.forEach(function(obj) {
          
          console.log("datafrom Api",obj.Name);
         
          var r=company.includes(obj.Name);
          if(!r)
          {
            company.push(obj);
           
            
          }
        
        });
        //company.push('All');
       // company.push('All');
        console.log("herecomp",company);
        this.setState({company:company})
        this.setState({gcheck:false})
        //var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/company/`+localStorage.getItem('com'))
         //console.log("udatafrom Api",res);
    /*    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user`)
        //console.log("datafrom Api",res);
        var fname;
        var lname;
        var x=[];
        res.data.Items.forEach(function(obj) {
          x.push(obj);
          if(localStorage.getItem('userEmail')===obj.EmailAddress)
          {
          console.log("fname Api",obj);
     //   x=obj.CompanyId
     localStorage.setItem("userId",obj.UserId);
        fname=obj.FirstName;
        lname=obj.LastName;
        localStorage.setItem("firstname",obj.FirstName);
        localStorage.setItem("lastname",obj.LastName);
        localStorage.setItem("uuid",obj.UserId);
          } 
        
        });*/
     //this.setState({users:x});
      this.setState({fname:fname[0]});
      this.setState({lname:fname[1]});

      }
    
console.log("comp",this.state.company);
    }
    catch(ex)
    {

    }
    
  
 
 
 }

 refresh=()=>{
 // var company=this.state.company;
 setInterval(async()=>{
  var company=this.state.company;
  var com=this.state.company;
  console.log("11Comaa",company)
  var i=0;
  var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company`)
  console.log("company datafrom Api",res.data);
  for(var i=0;i<company.length;i++)
  {
  res.data.Items.forEach(function(obj) {
    
    console.log("datafrom Api",obj.Name);
   try
   {    //var r=company[i].Name.includes(obj.Name);
  // console.log("111com",r)
    if(company[i].Name!=obj.Name)
    {
      com.push(obj);
     console.log("211com",obj.Name)
      
    }
   }
   catch(e)
   {
     console.log(e)
   }
  
  });
}
  //company.push('All');
 // company.push('All');

  console.log("22herecomp",com);
  this.setState({company:com})

 },10000);
 
      
    
  
  }
 
 sel=async (event)=>{
  console.log("22event",event);
console.log("search",this.state.users);
var searcharray=this.search(this.state.users,event);
console.log("search",searcharray);
this.setState({susers:searcharray})
console.log("usearch",this.state.susers);




try
{

  var res1= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/company/`+event)
  console.log('res1',res1);
      var x=[];
      res1.data.Items.forEach(function(obj) {

x.push(obj);
      })
this.setState({susers:x});
var res=await apiCall.getApi(`/company/getlogo/${event}`);
this.setState({logoimg:"data:image/*;base64," + res.data.base64 })
}
catch(err)
{
  this.setState({logoimg:"" })
  console.log(err);
}
 this.props.func(event)

}
search(source, nam){
  var results;
  //console.log("setcompcall",nam)  
  //console.log("res"+JSON.stringify(source));
  //nam = nam;
  results = source.filter(function(entry) {
      return entry.CompanyId.indexOf(nam) !== -1;
  });
  console.log("res"+JSON.stringify(results));
        return results;
}
  languageMenu = () => (
    <CustomScrollbars className="gx-popover-lang-scroll">
      <ul className="gx-sub-popover">
        {languageData.map(language =>
          <li className="gx-media gx-pointer" key={JSON.stringify(language)} onClick={(e) =>
            this.props.switchLanguage(language)
          }>
            <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`}/>
            <span className="gx-language-text">{language.name}</span>
          </li>
        )}
      </ul>
    </CustomScrollbars>);

  updateSearchChatUser = (evt) => {
    this.setState({
      searchText: evt.target.value,
    });
  };

  usermenu = () => (
    <Menu>
       {this.state.susers.map((x,y) => <Menu.Item style={{marginRight: "0px"}}>
     
      <Avatar  style={{ backgroundColor:"rgb(24, 144, 255)"}} icon={<UserOutlined style={{marginRight: "0px"}}/>} /> {x.FirstName[0].toUpperCase() +  
            x.FirstName.slice(1)} {x.LastName[0].toUpperCase() +  
              x.LastName.slice(1)}
      
    </Menu.Item> )}
    
   
  </Menu>
  );

  render() {
    const {locale, width, navCollapsed, navStyle} = this.props;
    return (
      <Auxiliary>
        <Header>
          {navStyle === NAV_STYLE_DRAWER || ((navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR) && width < TAB_SIZE) ?
            <div className="gx-linebar gx-mr-3">
              <i className="gx-icon-btn icon icon-menu"
                 onClick={() => {
                   this.props.toggleCollapsedSideNav(!navCollapsed);
                 }}
              />
            </div> : null}
          <Link to="/" className="gx-d-block gx-d-lg-none gx-pointer">
            <img alt="" src={require("assets/images/w-logo.png")}/></Link>

          {this.state.logoimg ?  <img src={this.state.logoimg}
            
            style={{height: "40px",
             
            width: "auto",
              marginLeft: "-29px",
              marginRight: "7px"}}/>:<p></p>}
            {this.state.gcheck?(<Input type="text" value={this.state.company[0]} style={{width: "196px"}} disabled/>):(
            <Select
    showSearch
    style={{ width: 200 }}
    placeholder="Select a Company"
    optionFilterProp="children"

    
    
   
    onSelect={this.sel}
    
  >
    <option  value='All'>ALL</option>
  {this.state.company.map((x,y) => <option key={x.CompanyId} >{x.Name}</option>)}
  
  </Select> )}
 
          <ul className="gx-header-notifications gx-ml-auto">
            <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
              {this.state.gcheck?(<Input type="text" value={this.state.company[0]}/>):(
              <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={
                      <SearchBox styleName="gx-popover-search-bar"
                           placeholder="Search in app..."
                           onChange={this.updateSearchChatUser.bind(this)}
                           value={this.state.searchText}/>
              } trigger="click">
                <span className="gx-pointer gx-d-block"><i className="icon icon-search-new"/></span>
              </Popover>)}
            </li>
            {width >= TAB_SIZE ? null :
              <Auxiliary>
                <li className="gx-notify">
                  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={<AppNotification/>}
                           trigger="click">
                    <span className="gx-pointer gx-d-block"><i className="icon icon-notification"/></span>
                  </Popover>
                </li>

                <li className="gx-msg">
                  <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
                           content={<MailNotification/>} trigger="click">
                  <span className="gx-pointer gx-status-pos gx-d-block">
                    <i className="icon icon-chat-new"/>
                    <span className="gx-status gx-status-rtl gx-small gx-orange"/>
                  </span>
                  </Popover>
                </li>
              </Auxiliary>
            }
            <li className="gx-language">
            <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row" style={{marginTop: "11px"}}>
     
     
        <span className="gx-avatar-name"></span>
  

      <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={this.usermenu()}
                       trigger="click">
                <span className="gx-pointer gx-flex-row gx-align-items-center">
               {this.state.susers.length>0?<AvatarGroup max={3} style={{fontSize:"0px", color:"transparent"}}>
          {this.state.susers.map((x,y) => <Avatar className="gx-size-40 gx-pointer "  style={{backgroundColor:"#1890ff",boxShadow: "3px 6px 7px -1px #888888"}}>{x.FirstName.charAt(0).toUpperCase()}{x.LastName.charAt(0).toUpperCase()}</Avatar>
             )}
               </AvatarGroup>:<p></p>}
                </span>
              </Popover>
             
    </div>
            </li>
            <li className="gx-user-nav"><UserInfo uprops={this.state}/></li>
            {width >= TAB_SIZE ? null :
              <Auxiliary>
                <li className="gx-user-nav"><UserInfo uprops={this.state}/></li>
              </Auxiliary>
            }
          </ul>
        </Header>
      </Auxiliary>
    );
  }
}

const mapStateToProps = ({settings}) => {
  const {locale, navStyle, navCollapsed, width} = settings;
  return {locale, navStyle, navCollapsed, width}
};

export default connect(mapStateToProps, {toggleCollapsedSideNav, switchLanguage})(Topbar);
