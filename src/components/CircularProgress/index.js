import React from "react";
import loader from "../../assets/images/395.gif"
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal} from 'antd';
const CircularProgress = ({className}) => <div className={`loader ${className}`}>
   <Spin />
</div>;
export default CircularProgress;
