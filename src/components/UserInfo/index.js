import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar, Popover } from "antd";
import { userSignOut } from "appRedux/actions/Auth";
import { Redirect } from "react-router-dom";
import { DatePicker, Switch, message } from "antd";
import IntlMessages from "util/IntlMessages";
import axios from "axios";

import {
  Card,
  Row,
  Col,
  Input,
  Modal,
  notification
} from "antd";
import { Form } from "antd";
import {
  PhoneOutlined,
  UserOutlined,
  MailOutlined
} from "@ant-design/icons";

import { CheckCircleOutlined, CloseCircleOutlined, LogoutOutlined } from "@ant-design/icons";

import AvatarGroup from "@material-ui/lab/AvatarGroup";

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

class UserInfo extends Component {
  constructor(props) {
    super(props);

   

    this.state = {
      collapsed: false,
      isAuthenticated: false,
      isAuthenticating: true,
      userId: "",
      username: "",
      Group: "",
      Userinfo: "",
      fname: "",
      lastname: "",
      dvisible: false,

      userDetail: {
        "UserId": null,
        "FirstName": null,
        "LastName": null,
        "MiddleName": null,
        "EmailAddress": null,
        "PhoneNum": null,
        "RoleId": null,
        "RoleName": null,
        "RoleDisplayName": null,
        "CompanyId": null,
        "CompanyName": null,
        "LastCompanyId": null,
        "DefaultCompanyId": null,
        "PrimaryCompanyId": null,
        "Expires": null,
        "IsEnabled": null,
        "CreatedOn": null,
        "CreatedBy": null,
        "LastModifiedBy": localStorage.getItem("userId")
      }
    };
  }

  async componentDidMount() {
    //   this.userHasAuthenticated(true);
    var user = localStorage.getItem("userEmail");
    var user_Id = localStorage.getItem("userId");
    this.setState({ Userinfo: user, UserId: user_Id });
    var uinfo = localStorage.getItem("user");
    //const user =  await Auth.currentAuthenticatedUser();
    //console.log(user.signInUserSession.accessToken.payload["cognito:groups"])
    this.setState({ Group: uinfo });
    //  this.props.history.push('/home')
    if (!user) {
      this.setState({ collapsed: true });
    }
    this.getUserInfo();
    console.log("fname", this.props);
  }

  getUserInfo() {
    var userId = localStorage.getItem("UserIdinfo");
    axios
      .get(
        "https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/" +
        userId
      )
      .then(res => {
        //this.companyDetails = res.data.Item;
        //res.data.Item.Website = res.data.Item.Website != null ? res.data.Item.Website.substring(4) : null;
        this.setState({ userDetail: res.data.Item });
      })
      .catch(err => {
        console.log(err);
      });
  }

  getusername() {
    var username = this.state.Userinfo;
    console.log("inside uname:" + username);
    var uname = username.split("@");
    return uname[0];
  }

  addmodal() {
    //alert("here");
    this.getUserInfo();
    this.setState({ dvisible: true, confirmLoading: false });
  }

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      dvisible: false,
      confirmLoading: false
    });
    // this.props.mvisible.rmodal(false);
  };

  updateUser = async () => {
    this.setState({
      ModalText: "The modal will be closed after two seconds",
      confirmLoading: true
    });
    var params = this.state.userDetail;

    try {
      var res = await axios.put(
        "https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user",
        params
      );

      if (res.data.Item) {
        setTimeout(() => {
          this.setState({
            dvisible: false,
            confirmLoading: false
          });
        }, 2000);

        notification.open({
          message: "Alert",
          description: `User ${this.state.userDetail.FirstName} updated successfully`,
          icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
        });

      } else {
        notification.open({
          message: "Alert",
          description: `${res.message.message}`,
          icon: <CloseCircleOutlined style={{ color: "red" }} />
        });

      this.setState({
        confirmLoading: false
      });
      }
    } catch (error) {
      notification.open({
        message: "Alert",
        description: error.message + " : Cannot update user",
        icon: <CloseCircleOutlined style={{ color: "red" }} />
      });
      this.setState({
        confirmLoading: false
      });
    }
  };

  handleLogout = async () => {
    //	await Auth.signOut();
    //this.setState({ isAuthenticated: false });
    //	this.userHasAuthenticated(false);
    console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
    localStorage.clear();
    this.setState({ username: "" });
    this.setState({ Group: "" });
    this.setState({ collapsed: true });
  };

  showMyProfile = async () => {
    console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
    localStorage.clear();
    this.setState({ username: "" });
    this.setState({ Group: "" });
    this.setState({ collapsed: true });
  };

  handleInputChange = event => {
    let fieldId = event.target.id;
    let value = event.target.value == "" ? null : event.target.value;

    this.setState(prevState => ({
      userDetail: {
        ...prevState.userDetail,
        [fieldId]: value
      }
    }));
  };

  render() {
    if (this.state.collapsed) {
      return <Redirect to="/signin" />;
    } else {
      const userMenuOptions = (
        // <ul className="gx-user-popover">
        //   <li onClick={() => this.addmodal(this)}>My Account</li>
        //   <li>Connections</li>
        //   <li onClick={() => this.handleLogout()}>Logout</li>
        // </ul>
        <ul className="gx-user-popover">
            <li className="gx-media gx-pointer" onClick={() => this.addmodal(this)}>
              <UserOutlined className="gx-mr-2" style={{ color: "#62B926" }} />
              <span className="gx-language-text">My Account</span>
            </li>
            <li
              className="gx-media gx-pointer"
              onClick={() => this.handleLogout()}
            >
              <LogoutOutlined
                className="gx-mr-2"
                style={{ color: "#f5222d" }}
              />
              <span className="gx-language-text">Logout</span>
            </li>
          </ul>
      );

      const content = (
        <div>
          <h4>User access to portal will expire on this date.</h4>
        </div>
      );

      return (
        <div>
          <div
            className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row"
            style={{ marginTop: "13px" }}
          >
            <Popover
              placement="bottomRight"
              content={userMenuOptions}
              trigger="click"
            >
              <Avatar
                className="gx-size-40 gx-pointer "
                style={{
                  backgroundColor: "#1890ff",
                  boxShadow: "3px 6px 7px -1px #888888"
                }}
                alt=""
              >
                {this.props.uprops.fname.charAt(0).toUpperCase()}
                {this.props.uprops.lname.charAt(0).toUpperCase()}
              </Avatar>
              <span className="gx-avatar-name">
                {" "}
                {this.getusername()}
                <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
              </span>
            </Popover>
          </div>
          <Modal
            bodyStyle={{ padding: "0" }}
            title=""
            visible={this.state.dvisible}
            onOk={this.updateUser}
            confirmLoading={this.state.confirmLoading}
            onCancel={this.handleCancel}
            width={900}
          >
            <div>
              <Card className="gx-card" title={
                <h2 style={{ color: "#1890ff" }}>
                  <IntlMessages id={"My Account"} />
                </h2>
              }
              style={{ marginBottom: "0px" }}
              >
                <Form
                  autocomplete="off"
                  onSubmit={this.handleSubmit}
                  style={{ marginLeft: "3px" }}
                >
                  <Row>
                    <Col span={12} className="colcustom">
                      <Form.Item className="fcust" label="First Name" {...formItemLayout}>
                        <Input
                          placeholder="First Name *"
                          id="FirstName"
                          value={this.state.userDetail.FirstName}
                          onChange={this.handleInputChange}
                          prefix={<UserOutlined />}
                          required
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item className="fcust" label="Last Name" {...formItemLayout}>
                        <Input
                          placeholder="Last Name *"
                          id="LastName"
                          value={this.state.userDetail.LastName}
                          onChange={this.handleInputChange}
                          prefix={<UserOutlined />}
                          required
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item className="fcust" label="Email" {...formItemLayout}>
                        <Input
                          type="email"
                          placeholder="Email Address *"
                          id="EmailAddress"
                          value={this.state.userDetail.EmailAddress}
                          prefix={<MailOutlined />}
                          disabled
                          required
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item className="fcust" label="Phone #"  {...formItemLayout}>
                        <Input
                          type="phone"
                          placeholder="Phone Number"
                          id="PhoneNum"
                          value={this.state.userDetail.PhoneNum}
                          onChange={this.handleInputChange}
                          prefix={<PhoneOutlined />}
                          required
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item label="Company"  {...formItemLayout}>
                        <Input
                          type="Company"
                          placeholder={this.state.companyname}
                          id="CompanyName"
                          value={this.state.userDetail.CompanyName}
                          prefix={<i className="icon icon-company" />}
                          disabled
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label="MFA"
                        {...formItemLayout}
                      >
                        <Switch
                        checkedChildren="Enable"
                        unCheckedChildren="Disable"
                        disabled/>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </Card>
            </div>
          </Modal>
        </div>
      );
    }
  }
}

export default connect(null, { userSignOut })(UserInfo);
