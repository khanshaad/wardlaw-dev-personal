import axios from 'axios';
let baseUrl=process.env.REACT_APP_APIBASEURL;

export default {
    async getApi(url,params) {
        let res;
        if(params)
        {
         res=await axios.get(`${baseUrl}${url}/${params}`);
        }
        else{
         res=await axios.get(`${baseUrl}${url}`);
        }
     
     return res;
    },
    async postApi(url,params) {
        let res;
        if(params)
        {
         res=await axios.post(`${baseUrl}${url}`,params);
        }
     
     return res;
    },
    async putApi(url,params) {
        let res;
        if(params)
        {
         res=await axios.put(`${baseUrl}${url}`,params);
        }
     return res;
    },
    async delete(url,params) {
        let res;
        if(params)
        {
         res=await axios.delete(`${baseUrl}${url}/${params}`);
        }
     return res;
    }
  };