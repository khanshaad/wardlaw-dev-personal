import React, {Component} from "react";
import { Card,Table } from 'antd';
import IntlMessages from "util/IntlMessages";
import axios from 'axios';
import './dashboard.css';

import moment from "moment";
import { AreaChartOutlined } from '@ant-design/icons';
class CompanyPage  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      comid:'',
      userdata:[]
    }
}
    async componentDidMount() {
       // var company=this.state.company;
       // var ainfo=[];
        var comid=localStorage.getItem('com');
        if(comid=='All')
        {
          comid='';
        }
        this.setState({comid});
        var ainfo=[];
        var params = {
            companyid:comid,
            "userid": localStorage.getItem('UserIdinfo'),
	"rolename": "WARD-Super-Admin"
        }
        var res= await axios.post(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/analytics/`,params)
        var y=res.data.Items.sort((a, b) => new Date(b.UploadedOn) - new Date(a.UploadedOn))
        console.log("y",y);
       y.forEach(function(obj) {
  
            console.log("datafrom Api",obj);
           ainfo.push(obj);

        });
        this.setState({userdata:ainfo});

    

    }

   custom_sort(a, b) {
      return new Date(a.UploadedOn).getTime() - new Date(b.UploadedOn).getTime();
  }

 ///console.log(props);
  render()
  {

    let columns = [
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'CompanyName',
            title: 'Company Name',
            dataIndex: 'CompanyName'
            },
        
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'UploadedBy',
                title: 'Uploaded By',
                dataIndex: 'UploadedBy'
                },
        {
        // company: 'Company Name',
        // Adminname: 'company',
        key: 'UploadedOn',
        title: 'Uploaded On',
        render: (text, item) => (
         <span>{moment(new Date(item.UploadedOn)).format("DD MMM YYYY")}  </span>
          ),
        },
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'CampaignName',
            title: 'Campaign Id',
            dataIndex: 'CampaignName'
            },
        
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'EndPointDeliveries',
                title: 'Count Of Email Delivered',
                dataIndex: 'EndPointDeliveries'
                },
                {
                    // company: 'Company Name',
                    // Adminname: 'company',
                    key: 'EmailDeliveryMetric',
                    title: 'Email Delivery Metric (%)',
                    dataIndex: 'EmailDeliveryMetric'
                    },
       
        {
        // company: 'Company Name',
        // Adminname: 'company',
        key: 'EmailBounceMetric',
        title: 'Email Bounce Metric (%)',
        dataIndex: 'EmailBounceMetric'
        }
        
        
          
              
                    
     
            ];
  return (
    <div>
       <Card style={{    borderRadius: "11px"}}>
    <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id="Dashboard"/></h2>

    <div className="gx-d-flex justify-content-center">
  
    </div>
    <div class="ant-row">
    <div class="ant-col ant-col-24" >




<Card title={<span style={{    paddingLeft: "23px"}}><AreaChartOutlined /> Outbound Email Analytics Data</span>} style={{fontSize: "12px"}}>

<Table dataSource={this.state.userdata} columns={columns}  pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}} size="small"/>


</Card>
</div>


</div></Card>
</div>
  

  
   
  );
};
}


export default CompanyPage;
