import React, {Component} from "react";
import { Card,Table,Steps,Button } from 'antd';
import IntlMessages from "util/IntlMessages";
import axios from 'axios';

import ReactPlayer from 'react-player'
import moment from "moment";
import { AreaChartOutlined, InfoCircleFilled } from '@ant-design/icons';
class Claims  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      comid:'',
      userdata:[],
      step1:'Step-1',
      step2:'Step-2',
      step3:'Step-3',
      count:0,
      control:true,
      url1:'',
      url2:'',
      url3:''
    }
}
    async componentDidMount() {
      this.setState({url1:'https://youtu.be/YKhYXUvmbwQ'})
       
    
    }

    handleProgress =async state => {
        console.log(state)
        this.setState({step1:"In-progress"})
        if(state.played===1)
        {
            this.setState({step1:"Finished"})
            this.setState({count:1});
            this.setState({url1:''})
            this.setState({url2:'https://youtu.be/DKhSRPULnsI'})  
        }
    }
        handleProgress2 =async state => {
            console.log(state)
            this.setState({step2:"In-progress"})
            if(state.played===1)
            {
                this.setState({step2:"Finished"})
                this.setState({count:2});
                this.setState({url2:''})  
                this.setState({url3:'https://https://youtu.be/8SxCCLk9nqE'})   

            }
        }
            handleProgress3 =async state => {
                console.log(state)
                this.setState({step3:"In-progress"})
                if(state.played===1)
                {
                    this.setState({step3:"Finished"})
                    this.setState({url3:''})   
                }
            }

            play=x=>
            {
                if(x==1)
                {
                    this.setState({url1:'https://youtu.be/YKhYXUvmbwQ'})
                }
                else if(x==2)
                {
                    this.setState({url2:'https://youtu.be/DKhSRPULnsI'})  
                }
                else if(x==3)
                {
                    this.setState({url3:'https://https://youtu.be/8SxCCLk9nqE'})   

                }
            }
  render()
  {
    const { Step } = Steps;
  
  return (
    <div>
       <Card style={{    borderRadius: "11px"}}>
    <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id={this.props.location.state.action}/></h2>

    <div className="gx-d-flex justify-content-center">
  
    </div>
    <div class="ant-row">
    <div class="ant-col ant-col-24" >




<Card title="">
    <h4>When a policyholder experience a loss, helping them understand the next steps is a crucial component to the claims experience.

Sending a Smart Claims Video helps customers understand the process, set expectation, and improves customer satisfaction.</h4>
<br/>
<Steps direction="vertical" current={this.state.count}>
    <Step title="Step-1" description={this.state.url1?<div style={{float:"right"}}><ReactPlayer url={this.state.url1}  controls={this.state.control} onProgress={this.handleProgress}  /></div>:<div style={{float:"right"}}><img src="https://outlook.office.com/actions/ei?u=http%3A%2F%2Fwardlaw-dev-landingpage.s3-website.us-east-2.amazonaws.com%2Femail_image.jpg&d=2020-05-08T04%3A59%3A25.908Z" height="350px" onClick={this.play(1)}/></div>} />
    <Step title="Step-2" description={this.state.url2?<ReactPlayer url={this.state.url2}  controls={this.state.control} onProgress={this.handleProgress2}  />:<img src="https://outlook.office.com/actions/ei?u=http%3A%2F%2Fwardlaw-dev-landingpage.s3-website.us-east-2.amazonaws.com%2Femail_image.jpg&d=2020-05-08T04%3A59%3A25.908Z" height="350px"  onClick={this.play(2)}/>} />
   
    <Step title="Step-3" description={this.state.url3?<div style={{float:"right"}}><ReactPlayer url={this.state.url3}  controls={this.state.control} onProgress={this.handleProgress3}  /></div>:<div style={{float:"right"}}><img src="https://outlook.office.com/actions/ei?u=http%3A%2F%2Fwardlaw-dev-landingpage.s3-website.us-east-2.amazonaws.com%2Femail_image.jpg&d=2020-05-08T04%3A59%3A25.908Z" height="350px"  onClick={this.play(3)}/></div>} />
   
  </Steps>

  <Button type="primary">Click Here To Learn More</Button> 
</Card>
</div>


</div></Card>
</div>
  

  
   
  );
};
}


export default Claims;
