import React from "react";
import {Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}dashboard`} component={asyncComponent(() => import('./DashboardPage'))}/>
    
      <Route path={`${match.url}user`} component={asyncComponent(() => import('./UserPage'))}/>
      <Route path={`${match.url}renewal`} component={asyncComponent(() => import('./Renewal'))}/>
      <Route path={`${match.url}report`} component={asyncComponent(() => import('./Reports'))}/>
      <Route path={`${match.url}create`}  component={asyncComponent(() => import('./create'))}/>
    <Route path={`${match.url}manage`}  component={asyncComponent(() => import('./manage'))}/>
    <Route path={`${match.url}CTA`}  component={asyncComponent(() => import('./CTAPage'))}/>
    <Route path={`${match.url}claims`}  component={asyncComponent(() => import('./Claims'))}/>
    </Switch>
  </div>
);

export default App;
