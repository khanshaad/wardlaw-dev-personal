import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip,DatePicker} from 'antd';
import "./dropdown.css";
import {
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const Datepicker = ({name,defaultval,placeholder,val,required,_handleChange}) => {
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="phone")
{
  x=<PhoneOutlined />
}
else if(name=="chtype")
{
  x=<PhoneOutlined />
}
  return (
    <Tooltip title={placeholder}>
    <div class="form-group">
    


    <DatePicker placeholder={placeholder} name={name} onChange={_handleChange} style={{width:"199px"}}
    required={required}
    />


    </div>
    </Tooltip>
  );
};

export default Datepicker;
